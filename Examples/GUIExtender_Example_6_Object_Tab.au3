#include <GUIConstantsEx.au3>
#include <IE.au3>

#include "GUIExtender.au3"

HotKeySet("^!a", "_Action_Section_1")
HotKeySet("^!b", "_Action_Section_3")

$hGUI = GUICreate("", 750, 770)
GUISetBkColor(0xC0C0C0)

_GUIExtender_Init($hGUI)

GUICtrlCreateLabel("Press Ctrl - F1 or Ctrl - F2 to toggle embedded objects", 10, 10, 730, 30)
GUICtrlSetFont(-1, 15)

$iSection_A = _GUIExtender_Section_Create($hGUI, 40, 370)
_GUIExtender_Section_Activate($hGUI, $iSection_A)
ConsoleWrite($iSection_A & @CRLF)

Global $oIE_1 = _IECreateEmbedded()
Global $hIE_1 = GUICtrlCreateObj($oIE_1, 10, 50, 730, 350)
_IENavigate ($oIE_1, "http://www.google.com")
_GUIExtender_Obj_Data($hIE_1, $oIE_1)

_GUIExtender_Section_Create($hGUI, Default, 170)

$cTab = GUICtrlCreateTab(10, 410, 730, 150)
ConsoleWrite("Main = " & GUICtrlGetHandle($cTab) & @CRLF)

$cTab0 = GUICtrlCreateTabItem("tab 0")

$cTab1 = GUICtrlCreateTabItem("tab 1")

GUICtrlCreateTabItem("")

$iSection_B = _GUIExtender_Section_Create($hGUI)
_GUIExtender_Section_Activate($hGUI, $iSection_B)
ConsoleWrite($iSection_B & @CRLF)

Global $oIE_2 = ObjCreate("Shell.Explorer.2")
Global $hIE_2 = GUICtrlCreateObj($oIE_2, 10, 570, 730, 180)
$oIE_2.navigate(@MyDocumentsDir)
_GUIExtender_Obj_Data($hIE_2, $oIE_2)

_GUIExtender_Section_Create($hGUI, -99)

_GUIExtender_Section_Action($hGUI, $iSection_A, 0)
_GUIExtender_Section_Action($hGUI, $iSection_B, 0)

GUISetState()

While 1
    $aMsg = GUIGetMsg(1)
    Switch $aMsg[0]
        Case $GUI_EVENT_CLOSE
            Exit
    EndSwitch
    _GUIExtender_EventMonitor($aMsg[1], $aMsg[0])
WEnd

Func _Action_Section_1()
    _GUIExtender_Section_Action($hGUI, $iSection_A, Not(_GUIExtender_Section_State($hGUI, $iSection_A)))
EndFunc

Func _Action_Section_3()
    _GUIExtender_Section_Action($hGUI, $iSection_B, Not(_GUIExtender_Section_State($hGUI, $iSection_B)))
EndFunc
