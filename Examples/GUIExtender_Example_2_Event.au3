#include <GUIConstantsEx.au3>

#include "../Includes/GUIExtender.au3"

Opt("GUIOnEventMode", 1)

$hGUI_1 = GUICreate("Vertical", 300, 390, 100, 100)
GUISetOnEvent($GUI_EVENT_CLOSE, "On_Exit")

_GUIExtender_Init($hGUI_1)

_GUIExtender_Section_Create($hGUI_1, Default, 60)
GUICtrlCreateGroup(" 1 - Static ", 10, 10, 280, 50)
_GUIExtender_Section_Activate($hGUI_1, 2, "", "", 270, 40, 15, 15, 0, 1) ; Normal button
GUICtrlCreateButton("Toggle All", 20, 25, 80, 20)
GUICtrlSetOnEvent(-1, "On_Button_1")

_GUIExtender_Section_Create($hGUI_1, Default, 110)
GUICtrlCreateGroup(" 2 - Extendable ", 10, 70, 280, 100)

_GUIExtender_Section_Create($hGUI_1, Default, 60)
GUICtrlCreateGroup(" 3 - Static", 10, 180, 280, 50)
_GUIExtender_Section_Activate($hGUI_1, 4, "Close 4", "Open 4", 225, 195, 60, 20, 1, 1) ; Push button

_GUIExtender_Section_Create($hGUI_1, Default, 60)
GUICtrlCreateGroup(" 4 - Extendable ", 10, 240, 280, 50)

_GUIExtender_Section_Create($hGUI_1);, 290, 90)
GUICtrlCreateGroup(" 5 - Static", 10, 300, 280, 80)
_GUIExtender_Section_Activate($hGUI_1, 0, "Close All", "Open All", 20, 340, 60, 20, 0, 1) ; Normal button

_GUIExtender_Section_Create($hGUI_1, -99)

GUICtrlCreateGroup("", -99, -99, 1, 1)

_GUIExtender_Section_Action($hGUI_1, 4, False)

GUISetState()

$hGUI_2 = GUICreate("Horizontal", 500, 200, 500, 100)
GUISetOnEvent($GUI_EVENT_CLOSE, "On_Exit")

_GUIExtender_Init($hGUI_2, 1)

_GUIExtender_Section_Create($hGUI_2, Default, 100)
GUICtrlCreateGroup(" 1 - Static ", 10, 10, 80, 180)
_GUIExtender_Section_Activate($hGUI_2, 2, "", "", 70, 20, 15, 15, 0, 1)
GUICtrlCreateButton("Toggle All", 20, 160, 60, 20)
GUICtrlSetOnEvent(-1, "On_Button_2")

_GUIExtender_Section_Create($hGUI_2, Default, 100)
GUICtrlCreateGroup(" 2 - Extendable ", 110, 10, 80, 180)

_GUIExtender_Section_Create($hGUI_2, Default, 100)
GUICtrlCreateGroup(" 3 - Static", 210, 10, 80, 180)
_GUIExtender_Section_Activate($hGUI_2, 4, "Close 4", "Open 4", 220, 90, 60, 20, 1, 1) ; Push button

_GUIExtender_Section_Create($hGUI_2, Default, 100)
GUICtrlCreateGroup(" 4 - Extendable", 310, 10, 80, 180)

_GUIExtender_Section_Create($hGUI_2, Default, 100)
GUICtrlCreateGroup(" 5 - Static", 410, 10, 80, 180)
_GUIExtender_Section_Activate($hGUI_2, 0, "Close All", "Open All", 420, 160, 60, 20, 0, 1) ; Normal button

_GUIExtender_Section_Create($hGUI_2, -99)

GUICtrlCreateGroup("", -99, -99, 1, 1)

_GUIExtender_Section_Action($hGUI_2, 2, False)

GUISetState()

While 1
	Sleep(10)
WEnd

Func On_Exit()
	Exit
EndFunc

Func On_Button_1()
	_GUIExtender_Section_Action($hGUI_1, 0, 9)
EndFunc

Func On_Button_2()
	_GUIExtender_Section_Action($hGUI_2, 0, 9)
EndFunc