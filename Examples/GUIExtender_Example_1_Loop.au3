#include <GUIConstantsEx.au3>

#include "../Includes/GUIExtender.au3"

$hGUI_1 = GUICreate("Vertical", 300, 390, 100, 100)

_GUIExtender_Init($hGUI_1)

_GUIExtender_Section_Create($hGUI_1, 0, 60)
GUICtrlCreateGroup(" 1 - Static ", 10, 10, 280, 50)
; Create action button for next section
_GUIExtender_Section_Activate($hGUI_1, 2, "", "", 270, 40, 15, 15) ; Normal button
; Create native controls within the section creation structure - before starting next section
$cButton_1 = GUICtrlCreateButton("Toggle All", 20, 25, 80, 20)

_GUIExtender_Section_Create($hGUI_1, Default, 110) ; using Default for position follows on from previous sections
GUICtrlCreateGroup(" 2 - Extendable ", 10, 70, 280, 100)

_GUIExtender_Section_Create($hGUI_1, -1, 60) ; As does -1
GUICtrlCreateGroup(" 3 - Static", 10, 180, 280, 50)
_GUIExtender_Section_Activate($hGUI_1, 4, "Close 4", "Open 4", 125, 195, 60, 20, 1) ; Push button

_GUIExtender_Section_Create($hGUI_1, -1, 60)
GUICtrlCreateGroup(" 4 - Extendable ", 10, 240, 280, 50)

_GUIExtender_Section_Create($hGUI_1) ; using the default values for both location and size fills the remainder of the GUI
GUICtrlCreateGroup(" 5 - Static", 10, 300, 280, 80)
; Create and action button for all sections
_GUIExtender_Section_Activate($hGUI_1, 0, "Close All", "Open All", 20, 340, 60, 20) ; Normal button

; Close section creation
_GUIExtender_Section_Create($hGUI_1, -99)

GUICtrlCreateGroup("", -99, -99, 1, 1)

_GUIExtender_Section_Action($hGUI_1, 4, 0)

GUISetState()

$hGUI_2 = GUICreate("Horizontal", 500, 200, 500, 100)

_GUIExtender_Init($hGUI_2, 1)

_GUIExtender_Section_Create($hGUI_2, 0, 100)
GUICtrlCreateGroup(" 1 - Static ", 10, 10, 80, 180)
_GUIExtender_Section_Activate($hGUI_2, 2, "", "", 70, 20, 15, 15)
$cButton_2 = GUICtrlCreateButton("Toggle All", 20, 160, 60, 20)

_GUIExtender_Section_Create($hGUI_2, 100, 100)
GUICtrlCreateGroup(" 2 - Extendable ", 110, 10, 80, 180)

_GUIExtender_Section_Create($hGUI_2, 200, 100)
GUICtrlCreateGroup(" 3 - Static", 210, 10, 80, 180)
_GUIExtender_Section_Activate($hGUI_2, 4, "Close 4", "Open 4", 220, 90, 60, 20, 1) ; Push button

_GUIExtender_Section_Create($hGUI_2, 300, 100)
GUICtrlCreateGroup(" 4 - Extendable", 310, 10, 80, 180)

_GUIExtender_Section_Create($hGUI_2, 400, 100)
GUICtrlCreateGroup(" 5 - Static", 410, 10, 80, 180)
_GUIExtender_Section_Activate($hGUI_2, 0, "Close All", "Open All", 420, 160, 60, 20) ; Normal button

_GUIExtender_Section_Create($hGUI_2, -99)

GUICtrlCreateGroup("", -99, -99, 1, 1)

_GUIExtender_Section_Action($hGUI_2, 2, 0)

GUISetState()

While 1
	$aMsg = GUIGetMsg(1)
	Switch $aMsg[0]
		Case $GUI_EVENT_CLOSE
			Exit
		Case $cButton_1
			_GUIExtender_Section_Action($hGUI_1, 0, 9) ; Mode 9 = toggle all moveable sections
		Case $cButton_2
			_GUIExtender_Section_Action($hGUI_2, 0, 9)
	EndSwitch

	_GUIExtender_EventMonitor($aMsg[1], $aMsg[0]) ; Check for click on section action control

WEnd