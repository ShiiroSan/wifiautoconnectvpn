#include <GUIConstantsEx.au3>
#include <GUIListBox.au3>

#include "../Includes/GUIExtender.au3"

Global $aGUI[1] = [0], $aButton[1] = [0], $aList[1] = [0]

_Create_GUI()

While 1
    $aMsg = GUIGetMsg(1)

    Switch $aMsg[0]
		Case $GUI_EVENT_CLOSE
			If $aMsg[1] = $aGUI[1] Then
				Exit
			Else
				For $i = 2 To $aGUI[0]
					If $aMsg[1] = $aGUI[$i] Then
						GUIDelete($aGUI[$i])
						; Clear GUI data from UDF
						_GUIExtender_Clear($aGUI[$i])
						ExitLoop
					EndIf
				Next
			EndIf
		Case Else
			; Was it a button
			For $i = 1 To $aButton[0]
				If $aMsg[0] = $aButton[$i] Then
					_Create_GUI()
					ExitLoop
				EndIf
			Next
		EndSwitch

		; Check for click on Action control
		_GUIExtender_EventMonitor($aMsg[1], $aMsg[0])

WEnd

Func _Create_GUI()

	Local $iCount = $aGUI[0]
	$iCount += 1

	ReDim $aGUI[$iCount + 1]
	$aGUI[0] = $iCount
	ReDim $aButton[$iCount + 1]
	$aButton[0] = $iCount
	ReDim $aList[$iCount + 1]
	$aList[0] = $iCount

	Local $hGUI = GUICreate("Test " & $iCount, 300, 300, 100 + (50 * $iCount), 100 + (50 * $iCount))
	$aGUI[$iCount] = $hGUI

	_GUIExtender_Init($hGUI)

	_GUIExtender_Section_Create($hGUI, 0, 100)
	GUICtrlCreateGroup(" 1 - Static ", 10, 10, 280, 80)
	_GUIExtender_Section_Activate($hGUI, 2, "", "", 270, 40, 15, 15) ; Normal button

	$iSection = _GUIExtender_Section_Create($hGUI, -1, 100)
	GUICtrlCreateGroup(" 2 - Extendable ", 10, 110, 280, 80)
	; No requirement to create this within the section as it is a UDF control
	$aList[$iCount] = _GUICtrlListBox_Create($hGUI, 'Test', 20, 130, 260, 50)
	_GUIExtender_Handle_Data($hGUI, $aList[$iCount], $iSection, 20, 130)

	_GUIExtender_Section_Create($hGUI, -1, -1)
	GUICtrlCreateGroup(" 3 - Static ", 10, 210, 280, 80)
	$aButton[$iCount] = GUICtrlCreateButton('New GUI', 20, 240, 80, 30)

	_GUIExtender_Section_Create($hGUI, -99)

	GUISetState(@SW_SHOW, $hGUI)

EndFunc