#include <GUIConstantsEx.au3>
#include <ProgressConstants.au3>
#include <SendMessage.au3>

#include "../Includes/GUIExtender.au3"

; Create GUI full size
$hGUI = GUICreate("Test", 300, 380)

; Create menu
$mFileMenu = GUICtrlCreateMenu("&File")
$mExit_Menu_Item = GUICtrlCreateMenuItem("&Exit", $mFileMenu)
$mViewMenu = GUICtrlCreateMenu("&View")
$mCode_Menu_Item = GUICtrlCreateMenuItem("&Menu Action Only", $mViewMenu)
$mMore_Menu_Item = GUICtrlCreateMenuItem("&More", $mViewMenu)
$mEdit_Menu_Item = GUICtrlCreateMenuItem("&Edit", $mViewMenu)

; Initialise UDF
_GUIExtender_Init($hGUI)

; Create the controls as normal - but add the section creation and activation lines where needed

$iCode_Section = _GUIExtender_Section_Create($hGUI, 0, 30)
; Here we are creating a action control for this section to be used programmatically - so no parameters required
_GUIExtender_Section_Activate($hGUI, $iCode_Section)
GUICtrlCreateLabel("GUIExtender UDF from Melba23.  I hope you like it!", 10, 10, 250, 20)

$iThis_Section = _GUIExtender_Section_Create($hGUI, -1, 50)
$cButton_1 = GUICtrlCreateButton("About", 20, 45, 80, 25)
GUICtrlCreateLabel("More...", 235, 60, 35, 15)
; Here we are creating an action control to appear on the GUI
_GUIExtender_Section_Activate($hGUI, $iThis_Section + 1, "", "", 270, 60, 15, 15) ; Note easy way to denote next section

$iMore_Section = _GUIExtender_Section_Create($hGUI, -1, 110) ; Save the section ID as a variable to use later
$cButton_2 = GUICtrlCreateButton("Sync", 20, 95, 80, 25)
GUIStartGroup()
$cRadio_1 = GUICtrlCreateRadio("Green", 120, 95, 60, 20)
GUICtrlSetState(-1, $GUI_CHECKED)
$cRadio_2 = GUICtrlCreateRadio("Red", 180, 95, 60, 20)
$cRadio_3 = GUICtrlCreateRadio("Yellow", 240, 95, 60, 20)
$cProgress = GUICtrlCreateProgress(25, 130, 250, 15)
GUICtrlSetData(-1, 50)
$cSlider = GUICtrlCreateSlider(25, 155, 250, 20)
GUICtrlSetData(-1, 50)

$iThis_Section = _GUIExtender_Section_Create($hGUI, -1, 140)
$cCombo = GUICtrlCreateCombo("", 20, 200, 260, 20)
GUICtrlSetData(-1, "Alpha|Bravo|Charlie|Delta", "Alpha")
$cList = GUICtrlCreateList("", 25, 240, 180, 80, 0x00200000)
GUICtrlSetData(-1, "One|Two|Three")
; Another action control to appear on the GUI
_GUIExtender_Section_Activate($hGUI, $iThis_Section + 1, "Close", "Input", 210, 290, 60, 20, 1)

$iInput_Section = _GUIExtender_Section_Create($hGUI, -1, -1)
; As this section was created with a default $iSectionStart parameter immediately after the previous one
; the exact coordinate used by the UDF is unknown - but it can be determined:
$iSection_BaseCoord = _GUIExtender_Section_BaseCoord($hGUI, $iInput_Section)
; And can then be used to correctly position controls within the section
$cInput = GUICtrlCreateInput("Your input goes here", 20, $iSection_BaseCoord, 180, 20)
$cButton_3 = GUICtrlCreateButton("OK", 210, $iSection_BaseCoord, 60, 20)

_GUIExtender_Section_Create($hGUI, -99)

GUICtrlCreateGroup("", -99, -99, 1, 1)

; Retract all extendable sections
_GUIExtender_Section_Action($hGUI, 0, 0)

GUISetState()

While 1

	$aMsg = GUIGetMsg(1)
	; Check for normal AutoIt controls
	Switch $aMsg[0]
		Case $GUI_EVENT_CLOSE, $mExit_Menu_Item
			Exit
		Case $mMore_Menu_Item
			If BitAND(GUICtrlRead($mMore_Menu_Item), $GUI_CHECKED) = $GUI_CHECKED Then
				_GUIExtender_Section_Action($hGUI, $iMore_Section, False)
				GUICtrlSetState($mMore_Menu_Item, $GUI_UNCHECKED)
			Else
				_GUIExtender_Section_Action($hGUI, $iMore_Section)
				GUICtrlSetState($mMore_Menu_Item, $GUI_CHECKED)
			EndIf
		Case $mEdit_Menu_Item
			If BitAND(GUICtrlRead($mEdit_Menu_Item), $GUI_CHECKED) = $GUI_CHECKED Then
				_GUIExtender_Section_Action($hGUI, $iInput_Section, False)
				GUICtrlSetState($mEdit_Menu_Item, $GUI_UNCHECKED)
			Else
				_GUIExtender_Section_Action($hGUI, $iInput_Section)
				GUICtrlSetState($mEdit_Menu_Item, $GUI_CHECKED)
			EndIf
		Case $mCode_Menu_Item
			If BitAND(GUICtrlRead($mCode_Menu_Item), $GUI_CHECKED) = $GUI_CHECKED Then
				_GUIExtender_Section_Action($hGUI, $iCode_Section, False)
				GUICtrlSetState($mCode_Menu_Item, $GUI_UNCHECKED)
			Else
				_GUIExtender_Section_Action($hGUI, $iCode_Section)
				GUICtrlSetState($mCode_Menu_Item, $GUI_CHECKED)
			EndIf
		Case $cButton_1
			MsgBox(0, "GUIExtender Demo", "Use the various buttons and menu items" & @CRLF & "to extend and retract hidden areas of the GUI")
		Case $cButton_2
			GUICtrlSetData($cProgress, GUICtrlRead($cSlider) + 1)
			GUICtrlSetData($cProgress, GUICtrlRead($cSlider))
		Case $cRadio_1
			_SendMessage(GUICtrlGetHandle($cProgress), $PBM_SETSTATE, 1)
		Case $cRadio_2
			_SendMessage(GUICtrlGetHandle($cProgress), $PBM_SETSTATE, 2)
		Case $cRadio_3
			_SendMessage(GUICtrlGetHandle($cProgress), $PBM_SETSTATE, 3)
		Case $cCombo
			GUICtrlSetData($cInput, GUICtrlRead($cCombo))
		Case $cButton_3
			GUICtrlSetData($cList, GUICtrlRead($cInput))
	EndSwitch

	; Check for GUIExtender controls
	_GUIExtender_EventMonitor($aMsg[1], $aMsg[0])

	; Keep menu items in sync with the section state
	If _GUIExtender_Section_State($hGUI, $iMore_Section) = 0 And BitAND(GUICtrlRead($mMore_Menu_Item), $GUI_CHECKED) = $GUI_CHECKED Then
		GUICtrlSetState($mMore_Menu_Item, $GUI_UNCHECKED)
	ElseIf _GUIExtender_Section_State($hGUI, $iMore_Section) = 1 And BitAND(GUICtrlRead($mMore_Menu_Item), $GUI_CHECKED) <> $GUI_CHECKED Then
		GUICtrlSetState($mMore_Menu_Item, $GUI_CHECKED)
	EndIf
	If _GUIExtender_Section_State($hGUI, $iInput_Section) = 0 And BitAND(GUICtrlRead($mEdit_Menu_Item), $GUI_CHECKED) = $GUI_CHECKED Then
		GUICtrlSetState($mEdit_Menu_Item, $GUI_UNCHECKED)
	ElseIf _GUIExtender_Section_State($hGUI, $iInput_Section) = 1 And BitAND(GUICtrlRead($mEdit_Menu_Item), $GUI_CHECKED) <> $GUI_CHECKED Then
		GUICtrlSetState($mEdit_Menu_Item, $GUI_CHECKED)
	EndIf

WEnd