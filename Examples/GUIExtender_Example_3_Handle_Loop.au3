#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>

#include <GUIComboBox.au3>

#include "GUIExtender.au3"

$hGUI = GUICreate("Test", 300, 390)

_GUIExtender_Init($hGUI)

_GUIExtender_Section_Create($hGUI, 0, 60)
GUICtrlCreateGroup(" 1 - Static ", 10, 10, 280, 50)
_GUIExtender_Section_Activate($hGUI, 2, "", "", 270, 40, 15, 15, 0, 1) ; Normal button

_GUIExtender_Section_Create($hGUI, Default, 60)
GUICtrlCreateGroup(" 2 - Extendable ", 10, 70, 280, 50)

_GUIExtender_Section_Create($hGUI, Default, 60)
GUICtrlCreateGroup(" 3 - Static", 10, 130, 280, 50)
_GUIExtender_Section_Activate($hGUI, 4, "Close 4", "Open 4", 225, 155, 60, 20, 1, 1) ; Push button

_GUIExtender_Section_Create($hGUI, Default, 110)
GUICtrlCreateGroup(" 4 - Extendable ", 10, 190, 280, 100)

_GUIExtender_Section_Create($hGUI)
GUICtrlCreateGroup(" 5 - Static", 10, 300, 280, 80)
_GUIExtender_Section_Activate($hGUI, 0, "Close All", "Open All", 20, 350, 60, 20, 1, 1) ; Normal button

_GUIExtender_Section_Create($hGUI, -99)

GUICtrlCreateGroup("", -99, -99, 1, 1)

GUISetState(@SW_HIDE, $hGUI)

; Create non-native controls and child GUIs AFTER main GUI

; Create a UDF combo
$hCombo = _GUICtrlComboBox_Create($hGUI, "", 120, 90, 60, 20, $CBS_DROPDOWN); BitOR($CBS_DROPDOWN, $WS_VSCROLL, $WS_TABSTOP, $CBS_UPPERCASE))
_GUICtrlComboBox_BeginUpdate($hCombo)
_GUICtrlComboBox_AddString($hCombo, "ONE")
_GUICtrlComboBox_AddString($hCombo, "TWO")
_GUICtrlComboBox_AddString($hCombo, "THREE")
_GUICtrlComboBox_EndUpdate($hCombo)
; Store handle data in UDF
_GUIExtender_Handle_Data($hGUI, $hCombo, 2, 120, 90) ; Note coords are relative to main GUI

; Create child GUI
$hGUI_Child = GUICreate("", 270, 80, 15, 205, $WS_POPUP)
GUISetBkColor(0xCCFFCC, $hGUI_Child)
GUISetState(@SW_SHOWNOACTIVATE, $hGUI_Child)
_WinAPI_SetParent($hGUI_Child, $hGUI)
_GUIExtender_Handle_Data($hGUI, $hGUI_Child, 4, 15, 205)

; Retract section with combo
_GUIExtender_Section_Action($hGUI, 4, False)

GUISetState(@SW_SHOW, $hGUI)

While 1
	$aMsg = GUIGetMsg(1)
	Switch $aMsg[0]
		Case $GUI_EVENT_CLOSE
			Exit
	EndSwitch

	_GUIExtender_EventMonitor($aMsg[1], $aMsg[0])

WEnd